---
title: "Postwork Sesión 1"
output: html_notebook
---
1. Se importaron los datos de soccer de la temporada 2019/2020 de la liga española del siguiente enlace: https://www.football-data.co.uk/spainm.php y se guardaron en una variable llamada "partidos_liga_española_df".

```{r}
partidos_liga_española_df <- read.csv("https://www.football-data.co.uk/mmz4281/2021/SP1.csv")
```

```{r}
(head(partidos_liga_española_df))
```

2. Se extraen las columnas llamadas "FTHG" y "FTAG" y se almacenan en las variables "casa" y "visitante" respectivamente. Posteriormente se guardan ambos en un solo dataframe llamado "goles".
```{r}
casa <- partidos_liga_española_df$FTHG

visitante <- partidos_liga_española_df$FTAG

matriz_goles <- table(goles)
```

3. Se consulta la función "table" de la librería "dplyr" con el comando "?table".
```{r}
?table
```
La función table cuenta las veces que se repite cierta combinacion de datos.

4. Para obtener la probabilidad marginal de que el equipo de casa anote x goles, primero obtenemos la frecuencia con la que anotan x cantidad de goles. Posteriormente este número se divide entre el total de partidos y se guarda en la variable "probabilidad_goles_casa".
```{r}
total_de_partidos <- length(casa)
frecuencia_por_goles_casa <- rowSums(matriz_goles)
probabilidad_goles_casa <- c(frecuencia_por_goles_casa / total_de_partidos)
```
  Análogamente para los goles de visitante
```{r}
frecuencia_por_goles_visitante <- colSums(matriz_goles)
probabilidad_goles_visitante <- c(frecuencia_por_goles_visitante / total_de_partidos)
```

Para obtener la probabilidad compuesta debemos multiplicar las probabilidades de cada uno de los goles de casa por cada una de las probabilidades de los goles de visitante. Para esto realizamos un producto exterior entre los elemtentos de "probabilidad_goles_casa" y "probabilidad_goles_visitante". Para esto descargamos la paquetería "tensor" y lo guardamos en la variable llamada "prob_compuesta".
```{r}
install.packages("tensor")

prob_compuesta <- as.data.frame(tensor::tensor(probabilidad_goles_casa, probabilidad_goles_visitante))
```

